# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- El Mazdoula, Zakarya, email: zakarya.elmazdoula.etu@univ-lille.fr

- Claeis, Nicolas, email: nicolas.claeis.etu@univ-lille.fr

## Question 1

Le processus ne peut écrire, car il n'a pas les permissions d'écrire en tant que propriétaire du fichier (premier triplet ici "r--").

## Question 2

L'autorisation d'exécution 'x', lorsqu'elle est définie pour un répertoire, donne la possibilité de parcourir son arborescence afin d'accéder aux fichiers ou aux sous-répertoires, mais pas de voir le contenu des fichiers à l'intérieur du répertoire (sauf si la lecture 'r' est définie sur les fichiers).

Toto n'a plus accès au répertoire "mydir", car les droits d'exécution ont été retirés.

Toto n'a pas accès au dossier mais voit uniquement son contenu car les droits d'exécution ont été retirés.

## Question 3

Nous avons d'abord : 1001 1001 1001 1001 Cannot open file
		
après avoir activer le flag set-user-id : 1000 1000 1000 1000
										  
et nous pouvons maintenant ouvrir le fichier.

## Question 4

Avant activation, les deux valaient 1001, mais nous ne pouvons maintenant pas activer le set-user-id. (toto is not in the sudoers file. This incident will be reported.) 
Aussi, si nous activons le set-user-id avec l'user "ubuntu", nous n'avons plus la permission de lancer de script avec toto.

## Question 5

La commande "chfn" nous permet de modifier les informations personnelles d'un utilisateur.
La commande nous renvoie : "-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn".
Ce qui signifie que le "root" peut lire, écrire et exécuté le fichier mais comme le flag "s" (set-id-user) est activé alors l'EUID doit être égal au propriétaire du fichier son exécution. 
Le groupe peut lire et exécuter, et enfin les "autres" peuvent le lire et l'exécuter.

En exécutant la commande chfn en suivant les questions avec l'utilisateur toto et en regardant dans le fichier /etc/passwd, nous avons pu voir que les informations ont bien été modifiées.

## Question 6

Les mots de passe sont stockés dans le fichier /etc/shadow. Ils sont cryptés et seul l'utilisateur root et les membres du groupe shadow peuvent y accéder. Les mots de passe ne sont pas mis dans le même ficher que /etc/passwd afin d'augmenter la sécurité en sont stockant dans /etc/shadow afin de ne pas être consulté par tout le monde.

## Question 7

Démarrer initilisation.sh pour mettre en place la structure et reset.sh pour revenir à l'état initial.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








