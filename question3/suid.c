#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *f;

    char ch;

    printf("%u \n",geteuid());
    printf("%u \n",getegid());
    printf("%u \n",getuid());
    printf("%u \n",getgid());


    f = fopen("mydir/mydata.txt","r");
    if (f == NULL)
    {
        printf("%s", "Cannot open file");
        exit(1);
    }

    while (ch=fgetc(f)!=EOF)
    {
        printf("%c",ch);
    }
    fclose(f);
    return 0;
}
